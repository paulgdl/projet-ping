<?php
/* Code permettant la connexion*/
  session_start();
  $titre = "Page d'accueil";
  include('all_nav.inc.php'); //Inclure la barre de navigation
  include('all_header.inc.php'); // Inclure l'entête

  if(isset($_SESSION['message'])) {
    echo '<div class="alert alert-primary" role="alert">';
    echo $_SESSION['message'];
    echo '</div>';
    unset($_SESSION['message']);
  }

  // Formulaire permettant de remplir les informations de connexion
?>

<form  method="POST" action="all_php_connexion.php">
  <div class="container">
  
    <div class="row">
      <div class="col-md-12">
        <label for="email" class="form-label">Email</label>
        <input type="email" class="form-control " id="email" name="email" placeholder="Votre email..." required>
      </div>
      <div class="col-md-12">
        <label for="password" class="form-label">Mot de passe</label>
        <input type="password" class="form-control " id="password" name="password" placeholder="Votre mot de passe..." required>
      </div>
    </div>
    <div class="row my-3">
      <div class="d-grid gap-2 d-md-block"><button class="btn btn-outline-primary" type="submit">Connexion</button></div>   
    </div>
  </div>

</form>
<?php
  include('all_footer.inc.php'); // Inclure le bas de page
?>

