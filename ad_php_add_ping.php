<?php
/* Code php permettant à un admin d'ajouter un responsable PING*/
  session_start(); // Pour les massages

  // récupere les valeurs saisis dans la page inscription

  if(!empty($_POST)){
  extract($_POST);
  $valid = true;
  }

  // Contenu du formulaire :
  if (isset($_POST['all_inscription.php'])){
    $nom =  htmlentities(trim['nom']);
    $prenom =  htmlentities(trim['prenom']);
    $email =  htmlentities(trim['email']);
    $password =  htmlentities(trim['password']);
  }

  // Pour dire que c'est un ping (role 3)
  $role = "3";

  include('all_fonction.php');    
  $mysqli = ConnexionBDD();
  
  // Permet de vérifier que l'email n'est pas déjà utiliser
  if(mysqli_num_rows(mysqli_query($mysqli,"SELECT * FROM tuteur WHERE EMAIL='$email'"))!=0){//si mysqli_num_rows retourne pas 0
    $_SESSION['message'] =  "Ce pseudo est déjà utilisé par un autre membre, veuillez en choisir un autre svp.";

} else {

  // Création du compte
  if ($stmt = $mysqli->prepare("INSERT INTO tuteur(PRENOM, NOM , EMAIL, PASS, ISVALIDATE) VALUES (?, ?, ?, ?, ?);")) {
    $encrypted = password_hash($password, PASSWORD_DEFAULT);

    $stmt->bind_param("sssss",$prenom,$nom,$email,$encrypted,$role);

    // Retourne le message de validation
    if($stmt->execute()) {
      $_SESSION['message'] = "Enregistrement réussi";

}}}
  header('Location: ad_page_add_ping.php'); // Redirection vers la page d'ajout d'un responsable PING
?>

<?php
  include('all_footer.inc.php') // Inclure le bas de page
?>
