<?php
  session_start();
/* Code php permettant l'inscription d'un utilisateur */


  // Récuperer les valeurs saisis dans la page inscription

  if(!empty($_POST)){
  extract($_POST);
  $valid = true;
  }

  // Contenu du formulaire :
  if (isset($_POST['all_inscription.php'])){
    $nom =  htmlentities(trim['nom']);
    $prenom =  htmlentities(trim['prenom']);
    $email =  htmlentities(trim['email']);
    $password =  htmlentities(trim['password']);
  }



  // Pour dire que le tuteur n'a pas été encore validé (assignation au rôle 0 de visiteur)
  $role = "0";

  include('all_fonction.php'); // Inclure la fonction de connexion
  $mysqli = ConnexionBDD();
  
  // Permet de vérifier que l'email n'est pas déjà utilisé

  if(mysqli_num_rows(mysqli_query($mysqli,"SELECT * FROM tuteur WHERE EMAIL='$email'"))!=0){ //si mysqli_num_rows retourne pas 0
    $_SESSION['message'] =  "Ce pseudo est déjà utilisé par un autre membre, veuillez en choisir un autre svp.";

} else {

  // Création du compte
  if ($stmt = $mysqli->prepare("INSERT INTO tuteur(PRENOM, NOM , EMAIL, PASS, ISVALIDATE) VALUES (?, ?, ?, ?, ?);")) {
    $encrypted = password_hash($password, PASSWORD_DEFAULT);

    $stmt->bind_param("sssss",$prenom,$nom,$email,$encrypted,$role);

    // Retourne le message de validation
    if($stmt->execute()) {
      $_SESSION['message'] = "Enregistrement réussi";

}}}
  header('Location: index.php'); // Redirection vers la page d'accueil 
?>

<?php
  include('all_footer.inc.php') // Inclure le bas de page
?>
