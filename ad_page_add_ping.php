<?php
/* Code permettant à un admin d'ajouter un responsable Ping*/
session_start();
  include('all_nav.inc.php'); // Inclure la barre de navigation
  include('all_header.inc.php'); // Inclure l'entête

  if(isset($_SESSION['message'])) {
    echo '<div class="alert alert-primary" role="alert">';
    echo $_SESSION['message'];
    echo '</div>';
    unset($_SESSION['message']);
  }

  // Sécurite de l'URL
  if(!isset($_SESSION['EMAIL']) || $_SESSION['ROLE'] != "5"){
    header("Refresh: 5; url=all_connexion.php");//redirection vers le formulaire de connexion dans 5 secondes
    echo "Vous devez vous connecter pour accéder à l'espace membre.<br><br><i>Redirection en cours, vers la page de connexion...</i>";
    exit(0);//on arrête l'éxécution du reste de la page avec exit, si le membre n'est pas connecté
}

// Formulaire permettant à un admin de remplir les informations du nouveau responsable PING
?>

<form action="ad_php_add_ping.php" method="post" >
  <div class="container">
    <div class="row my-3">
      <div class="col-md-6">
        <label for="nom" class="form-label">Nom</label>
        <input type="text" class="form-control " id="nom" name="nom" placeholder="Votre nom..." required>
      </div>
      <div class="col-md-6">
        <label for="prenom" class="form-label">Prénom</label>
        <input type="text" class="form-control " id="prenom" name="prenom" placeholder="Votre prénom..." required>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <label for="email" class="form-label">Email</label>
        <input type="email" class="form-control " id="email" name="email" placeholder="Votre email..." required>
      </div>
      <div class="col-md-6">
        <label for="password" class="form-label">Mot de passe</label>
        <input type="password" class="form-control " id="password" name="password" placeholder="Votre mot de passe..." required>
      </div>
    </div>
    <div class="row my-3">
      <div class="d-grid gap-2 d-md-block"><button class="btn btn-outline-primary" type="submit">Co</button></div>   
    </div>
  </div>

</form>


<?php
  include('all_footer.inc.php') // Inclure le bas de page
?>
