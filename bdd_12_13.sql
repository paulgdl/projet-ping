-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:8889
-- Généré le : dim. 13 nov. 2022 à 14:47
-- Version du serveur : 5.7.34
-- Version de PHP : 7.4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `bdd_12_13`
--

-- --------------------------------------------------------

--
-- Structure de la table `blog`
--

CREATE TABLE `blog` (
  `TITRE` varchar(30) NOT NULL,
  `TEXTE` varchar(200) NOT NULL,
  `ISREADY` varchar(30) NOT NULL,
  `EMAIL` varchar(30) NOT NULL,
  `pdf` varchar(50) NOT NULL,
  `image` varchar(50) NOT NULL,
  `acess` varchar(30) NOT NULL,
  `modification` varchar(100) NOT NULL,
  `suppression` varchar(100) NOT NULL,
  `ismodifier` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `tuteur`
--

CREATE TABLE `tuteur` (
  `PRENOM` varchar(30) NOT NULL,
  `NOM` varchar(30) NOT NULL,
  `EMAIL` varchar(30) NOT NULL,
  `PASS` varchar(300) NOT NULL,
  `ISVALIDATE` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `tuteur`
--

INSERT INTO `tuteur` (`PRENOM`, `NOM`, `EMAIL`, `PASS`, `ISVALIDATE`) VALUES
('admin', 'admin', 'admin@admin', '$2y$10$ab0CEeGDQdLE8YrZXTybDOZkC2KX94cKT8vkgRrpTQkU7RkoGOBdO', '5'),
('cochereau', 'theo', 'theo@chr', '$2y$10$OiHWnJX9ulxCgh6oxcbwPuliTpPfANeyElTj3y5hmVfJ96DCDGl5K', '3'),
('Grassin', 'paul', 'paul@gra', '$2y$10$S.7HAhY2y5RpyNR8fUmnKeiZglsDd1q7urLEjvx1GJPBihpRYf4.C', '1'),
('Prenom', 'Nom', 'inscrit@inscrit', '$2y$10$lNZMLxLWwA8xe3tR9aEYZuX58OVg3CbtVhfQrAryvhVOp0G4YMynG', '0');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
