<?php
/* Code php permettant à un tuteur d'ajouter un nouvel article */
  session_start();

  // Récuperer les valeurs saisis dans la page inscription
  if(!empty($_POST)){
  extract($_POST);
  $valid = true;
  }

  // Contenu du formulaire :
  if (isset($_POST['tt_page_ajout_blog.php'])){
    $titre =  htmlentities(trim['titre']); //suprime les espaces
    $text =  htmlentities(trim['text']);
  }

  /* IMAGE TELECHARGEMENT */

// Uploader l'image
$target_dir = "image/";
$target_file = $target_dir . basename($_FILES["imageToUpload"]["name"]);
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));


// Vérifier la conformité de l'image
if(isset($_POST["tt_page_ajout_blog.php"])) {
  $check = getimagesize($_FILES["imageToUpload"]["tmp_name"]);
  if($check !== false) {
    $_SESSION['file_image'] =  "File is an image - " . $check["mime"] . ".";
    $uploadOk = 1;
  } else {
    $_SESSION['file_image'] = "File is not an image.";
    $uploadOk = 0;
  }
}

// Vérifier si le fichier existe
if (file_exists($target_file)) {
  $_SESSION['exist_image'] = "Votre image existe déjà, Veuillez changer de nom.";
  $uploadOk = 0;
}

// Autoriser les formats jpg, png et jpeg
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg") {
  $_SESSION['format_image'] =  "Désole, seul les jpg, png et jpeg sont autorisés";
  $uploadOk = 0;
}

// Vérifier si $uploadOk est sur 0 et si oui, renvoyer une erreur
if ($uploadOk == 0) {
  $_SESSION['file_image'] =  "désole, votre image n'a pas été transmis.";
// Si toutes les verifications sont ok, uploader l'image
} else {
  if (move_uploaded_file($_FILES["imageToUpload"]["tmp_name"], $target_file)) {
    $_SESSION['file_image'] =  "The file ". htmlspecialchars( basename( $_FILES["imageToUpload"]["name"])). " has been uploaded.";
  } else {
    $_SESSION['file_image'] =  "désole, du à une erreur, votre image n'a pas été transmis";
  }
}

$image = "image/".$_FILES["imageToUpload"]["name"];

/* PDF TELECHARGEMENT */


// Uploader le fichier pdf
$target_dir = "pdf/";
$target_file = $target_dir . basename($_FILES["pdfToUpload"]["name"]);
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

// Vérifier la conformité du fichier
if(isset($_POST["tt_page_ajout_blog.php"])) {
  $check = getimagesize($_FILES["pdfToUpload"]["tmp_name"]);
  if($check !== false) {
    $_SESSION['file_pdf'] =   "File is an image - " . $check["mime"] . ".";
    $uploadOk = 1;
  } else {
    $_SESSION['file_pdf'] = "File is not an image.";
    $uploadOk = 0;
  }
}

// Vérifier si le fichier existe
if (file_exists($target_file)) {
  $_SESSION['exist_pdf'] = "Votre pdf existe déjà, Veuillez changer de nom.";
  $uploadOk = 0;
}

// Autoriser le format pdf
if($imageFileType != "pdf") {
  $_SESSION['format_pdf'] =  "Désole, seul les pdf sont autorisés";
  $uploadOk = 0;
}


// Vérifier si $uploadOk est sur 0 et si oui, renvoyer une erreur
if ($uploadOk == 0) {
  $_SESSION['file_pdf'] = "désole, votre pdf n'a pas été transmis.";
// Si toutes les verifications sont ok, uploader le fichier
} else {
  if (move_uploaded_file($_FILES["pdfToUpload"]["tmp_name"], $target_file)) {
    $_SESSION['file_pdf'] = "The file ". htmlspecialchars( basename( $_FILES["fileToUpload"]["name"])). " has been uploaded.";
  } else {
    $_SESSION['file_pdf'] = "désole, du à une erreur, votre pdf n'a pas été transmis";
  }
}

$pdf = "pdf/".$_FILES["pdfToUpload"]["name"];


  // Ajout à valider par un ping
  $isReady = "0";
  $access="";
  $modification="";
  $suppression="";
  $ismodifier="0";

  include('all_fonction.php');    
  $mysqli = ConnexionBDD();
  
  // Attention, ici on ne vérifie pas si l'utilisateur existe déjà
  if ($stmt = $mysqli->prepare("INSERT INTO blog(TITRE, TEXTE , ISREADY, EMAIL, pdf, image, acess, modification, suppression, ismodifier) VALUES 
  ('".$titre."', '".$text."','".$isReady."','".$_SESSION['EMAIL']."','".$pdf."','".$image."','".$access."','".$modification."','".$suppression."','".$ismodifier."');")) {

    if($stmt->execute()) {
      $_SESSION['message'] = "Enregistrement du blog réussi";
    } else{
        $_SESSION['message'] = "Echec du blog réussi";
    }
}
?>
<?php
  header('Location: tt_page_blog.php'); // Redirection vers la page d'accès aux blogs

?>




