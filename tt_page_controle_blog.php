<?php
/* Code permettant à un tuteur de voir si son article a été validé ou non, de voir si un responsable PING a fait une demande de mofication et quelle est cette demande, de supprimer son article s'il n'a pas été validé et de voir également si le responsable a faire une demande de suppression et pour quelle raison */
  session_start();
  include('all_nav.inc.php'); // Inclure la barre de navigation
  include('all_header.inc.php'); // Inclure l'entête
  include('all_fonction.php');  // Inclure la fonction de connexion  
  
  if(isset($_SESSION['message'])) {
    echo '<div class="alert alert-primary" role="alert">';
    echo $_SESSION['message'];
    echo '</div>';
    unset($_SESSION['message']);
  }




  // Connexion :
  $mysqli = ConnexionBDD(); 


// Sécurité de l'url 
if(!isset($_SESSION['EMAIL']) || $_SESSION['ROLE'] != "1"){
  header("Refresh: 5; url=all_connexion.php");//redirection vers le formulaire de connexion dans 5 secondes
  echo "Vous devez vous connecter pour accéder à l'espace membre.<br><br><i>Redirection en cours, vers la page de connexion...</i>";
    exit(0);//on arrête l'éxécution du reste de la page avec exit, si le membre n'est pas connecté
}

//Crée une variable qui content les valeurs de la bdd
$reponse = $mysqli->query("SELECT * FROM blog WHERE EMAIL='".$_SESSION['EMAIL']."'");

?>

<!doctype html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Page d'acceuil</title>

    <!-- css & bootstrap-->

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">

  </head>

  <!-- contenue du site -->

    <body>

    <section class="container-fluid about">
        <div class="container">

        <div class="row">
          <h2 id="about">Modifier mes blogs</h2>
          <hr class="seperator">
          <article class="col-md-12 col-lg-12 col-xs-12 col-sm-12">

          
          <?php
    while ($donnees = $reponse->fetch_assoc())
    {

      ++$a;
        $_SESSION['TITRE'.$a] = $donnees['TITRE'];
      
    ?>
    <p>
              <?php 
                if(!$donnees['suppression']){
                  if ($donnees['ISREADY'] == '0'){
                    echo "[NON VALIDE]";
                  } else {
                    echo "[VALIDE]";
                  }
                  ?>
</p>
<p>
<?php
             if($donnees['ismodifier'] == "1"){
                    echo "Les modifications à effectuées pour publication : ";
                    echo $donnees['modification'];

                  }
              ?>
              </p>

                 <h2> Titre : <?php echo $donnees['TITRE'] ?> </h2>
<?php
                 if ($donnees['image'] != "image/"){

?>
 <article class="col-md-12 col-lg-12 col-xs-12 col-cm-12">
<section class="container-fluid">
  <a><img src="<?php echo $donnees['image'] ?>"></a>
</section>          
</article>

<?php } ?>

                 <p> texte : <?php echo $donnees['TEXTE']?></p>

                 <?php
            
            if ($donnees['pdf'] != "pdf/"){
              ?>
               <a href="<?php echo $donnees['pdf'] ?>">Clique pour télécharger le pdf</a> 
            <?php } ?>

                 
            <form action="tt_php_controle_blog.php" method="post" >
            <?php if($donnees['ISREADY'] == "0"){?>
          
          <div class="row my-3">
           <td><span class="style1">
              <input type="submit" name="action" id="modifier" value=<?php echo "Modifier".$a ?> />
            </span></td>
          </div>
          <?php } ?>

          <?php } ?>

          <?php
            
              if ($donnees['suppression']){

                ?> 

                <h2> <?php echo $donnees['TITRE'] ?> </h2>
                 <p><?php echo $donnees['TEXTE']?></p>

                 <?php
                
                echo "Votre sujet va être supprimer car :";
                echo $donnees['suppression'];
              }

            ?>
            <form action="tt_php_controle_blog.php" method="post" >
          
          
          <div class="row my-3">
           <td><span class="style1">
           <?php if($donnees['ISREADY'] == "0"){?>
            
           <td><input type="submit" name="action" id="supprimer" value=<?php echo "Supprimer".$a ?> /></td>
           <?php } ?>
            </span></td>
         
          
          </div>
          <form>

            <hr class="seperator">

            <?php
    
            }
  
  ?>
        </div>
      </div>
      </section>
    <body>
<html>

<?php
  include('all_footer.inc.php') // Inclure le bas de page
?>

