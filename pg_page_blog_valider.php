<?php
/* Code permettant à un responsable PING de supprimer un article qui a déjà été validé */
  session_start();
  include('all_nav.inc.php'); // Inclure la barre de navigation
  include('all_header.inc.php'); // Inclure l'entête
  include('all_fonction.php'); // Inclure la fonction de connexion
  
  

  if(isset($_SESSION['message'])) {
    echo '<div class="alert alert-primary" role="alert">';
    echo $_SESSION['message'];
    echo nl2br("\nBienvenue : ".$_SESSION['NOM']." ".$_SESSION['PRENOM']."\n Vous êtes connecté en mode ping\n");

    echo '</div>';
    unset($_SESSION['message']);
  }

 
  
  
    // Connexion :
    $mysqli = ConnexionBDD(); 
  
    // Sécurité de l'url 
  if(!isset($_SESSION['EMAIL']) || $_SESSION['ROLE'] != "3"){
    header("Refresh: 5; url=all_connexion.php");//redirection vers le formulaire de connexion dans 5 secondes
    echo "Vous devez vous connecter pour accéder à l'espace membre.<br><br><i>Redirection en cours, vers la page de connexion...</i>";
    exit(0);//on arrête l'éxécution du reste de la page avec exit, si le membre n'est pas connecté
}

$reponse = $mysqli->query("SELECT * FROM blog");


?>

<!doctype html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Page des blogs</title>

    <!-- css & bootstrap-->

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">

  </head>

  <!-- contenue du site -->

    <body>

    <section class="container-fluid about">
        <div class="container">

        <div class="row">
          <h2 id="about">Les blogs validés</h2>
          <hr class="seperator">

          <?php
    while ($donnees = $reponse->fetch_assoc())
    {
        ++$a;
        $_SESSION['TITRE'.$a] = $donnees['TITRE'];
        if ($donnees['ISREADY'] == '1'){ // Si l'article a été validé
    ?>
          <article class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
            <h2> Titre : <?php echo $donnees['TITRE'] ?> </h2>
            <p> texte : <?php echo $donnees['TEXTE'] ?> </p>

            <form action="pg_php_blog_valider.php" method="post" >

<div class="row my-3">
 <td><span class="style1"> 
    <input type="submit" name="action" id="supprimer" value=<?php echo "Supprimer".$a ?> /> 
  </span></td> 

</div>
<form>


            <hr class="seperator">

            <?php
    }}
  ?>
        </div>
      </div>

      </section>

    <body>
<html>

<?php
  include('all_footer.inc.php') // Inclure le bas de page
?>


