
<?php
/* Code php permettant à un tuteur de modifier son article s'il n'a pas été validé */
  session_start(); // Pour les messages

  if(!empty($_POST)){
    extract($_POST);
    $valid = true;
    }
?>

<?php

if (isset($_POST['tt_page_update_blog.php'])){
  $text =  htmlentities(trim['text']); // Récupérer le texte du nouvel article
}

include('all_fonction.php'); // Inclure la fonction de connexion
$mysqli = ConnexionBDD();

if ($stmt = $mysqli->prepare("UPDATE blog SET texte = '".$text."' WHERE TITRE='".$_SESSION['MODIFICATION']."'")) { // Modifier le blog avec le nouveau texte dans la base de données

    if($stmt->execute()) {
      $_SESSION['message'] = "Modification du blog réussit";
    } else{
        $_SESSION['message'] = "Echec de modification du blog";
    }
    header('Location: Blogs.php');
}

if ($stmt = $mysqli->prepare("UPDATE blog SET ismodifier = '2' WHERE TITRE='".$_SESSION['MODIFICATION']."'")) { // Informer le responsable PING que le blog a été modifié

  if($stmt->execute()) {
    $_SESSION['message'] = "Modification du blog réussit";

  } else{
      $_SESSION['message'] = "Echec de modification du blog";
  }
  header('Location: tt_page_controle_blog.php'); // Redirection vers la page d'accès aux blogs
}
?>