<?php
    /*************************
    * Page: deconnexion.php
    * Page encodée en UTF-8
    **************************/
/* Code permettant le deconnexion */ 
session_start();
unset($_SESSION['EMAIL']);//unset() détruit une variable, si vous enregistrez aussi l'id du membre (par exemple) vous pouvez comme avec isset(), mettre plusieurs variables séparés par une virgule:
//unset($_SESSION['pseudo'],$_SESSION['id']);
unset($_SESSION['ROLE']);
unset($_SESSION['message']);
unset($_SESSION['valide']);

unset($_SESSION['file_image']);
unset($_SESSION['exist_image']);
unset($_SESSION['format_image']);
unset($_SESSION['file_pdf']);
unset($_SESSION['exist_pdf']);

header("Refresh: 1; url=./");//redirection vers le formulaire de connexion dans 5 secondes
echo "Vous avez été correctement déconnecté du site.<br><br><i>Redirection en cours, vers la page d'accueil...</i>";
?>