<?php
/* Code permettant à un utilsateur de s'inscrire*/
  $titre = "Page d'inscription";
  include('all_nav.inc.php'); //Inclure la barre de navigation
  include('all_header.inc.php'); // Inclure l'entête

  /* Formulaire permettant à l'utilisateur de compléter les informations d'inscription*/
?>



<form action="all_php_inscription.php" method="post" >
  <div class="container">
    <div class="row my-3">
      <div class="col-md-6">
        <label for="nom" class="form-label">Nom</label>
        <input type="text" class="form-control " id="nom" name="nom" placeholder="Votre nom..." required>
      </div>
      <div class="col-md-6">
        <label for="prenom" class="form-label">Prénom</label>
        <input type="text" class="form-control " id="prenom" name="prenom" placeholder="Votre prénom..." required>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <label for="email" class="form-label">Email</label>
        <input type="email" class="form-control " id="email" name="email" placeholder="Votre email..." required>
      </div>
      <div class="col-md-6">
        <label for="password" class="form-label">Mot de passe</label>
        <input type="password" class="form-control " id="password" name="password" placeholder="Votre mot de passe..." required>
      </div>
    </div>
    <div class="row my-3">
      <div class="d-grid gap-2 d-md-block"><button class="btn btn-outline-primary" type="submit">Inscription</button></div>   
    </div>
  </div>

</form>


<?php
  include('all_footer.inc.php') // Inclure le bas de page
?>
