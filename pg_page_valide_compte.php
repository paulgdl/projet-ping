<?php
/* Code permettant à un responsable PING de valider un compte visiteur (rôle 0) venant de s'inscrire pour le promouvoir en tuteur (rôle 1)*/
  session_start();
  include('all_nav.inc.php'); // Inclure la barre de navigation
  include('all_header.inc.php'); // Inclure l'entête

  if(isset($_SESSION['message'])) {
    echo '<div class="alert alert-primary" role="alert">';
    echo $_SESSION['valide'];
    echo '</div>';
    unset($_SESSION['valide']);
  }

  




  
    // Connexion :
    include('all_fonction.php'); // Inclure la fonction de connexion   
    $mysqli = ConnexionBDD(); 
  
    // Sécurité de l'url 

    if(!isset($_SESSION['EMAIL']) || $_SESSION['ROLE'] != "3"){
      header("Refresh: 5; url=all_connexion.php");//redirection vers le formulaire de connexion dans 5 secondes
      echo "Vous devez vous connecter pour accéder à l'espace membre.<br><br><i>Redirection en cours, vers la page de connexion...</i>";
    exit(0);//on arrête l'éxécution du reste de la page avec exit, si le membre n'est pas connecté
}

$reponse = $mysqli->query("SELECT * FROM tuteur");


?>

<!doctype html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Page des blogs</title>

    <!-- css & bootstrap-->

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">

  </head>

  <!-- contenue du site -->

    <body>
    <section class="container-fluid about">
        <div class="container">

        <div class="row">
          <h2 id="about">Compte à valider</h2>
          <hr class="seperator">

          <?php
    while ($donnees = $reponse->fetch_assoc())
    {
        if ($donnees['ISVALIDATE'] == '0'){ // Si le compte n'a pas été encore validé

        ++$a;
        $_SESSION['EMAIL'.$a] = $donnees['EMAIL'];
        
    ?>
          <article class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
            <h2>Compte inscris</h2>
            <p> <?php echo " email : ".$donnees['EMAIL'] ?> </p>
            <p> <?php echo " nom : ".$donnees['NOM'] ?> </p>
            <p> <?php echo "prenom : ".$donnees['PRENOM'] ?> </p>
            <p> <?php echo " compte pas encore validé" ?> </p>

            <form action="pg_php_valide_compte.php" method="post" >

<div class="row my-3">
 <td><span class="style1"> 
    <input type="submit" name="action" id="accepter" value=<?php echo "Accepter".$a?> /> 
  </span></td> 

</div>
<form>


<div class="row my-3">
 <td><span class="style1"> 
 <td><input type="submit" name="action" id="refuser" value=<?php echo "Refuser".$a ?> /></td> 
  </span></td> 

</div>
<form>

<hr class="seperator">

            <?php
        }
    }
    
    
  ?>

<html>

<?php
  include('all_footer.inc.php') // Inclure le bas de page 
?>


