      <!-- footer-->
      <!doctype html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Footer</title>

    <!-- css & bootstrap-->

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
    <link rel="stylesheet" href="all_style.css">

  </head>

      <section class="container-fluid portfolio">
        <div class="container">
          <div class="row">

          <h2 id="portfolio">Nous Contacter</h2>
          <hr class="seperator">

          <article class="col-md-3 col-lg-3 col-xs-12 col-cm-12">

              <section class="container-fluid">
                <a href="http://instagram.com"><img src="assets/insta.png" alt="lien RS"></a>
              </section>          
          </article>

          <article class="col-md-3 col-lg-3 col-xs-12 col-cm-12">

            <section class="container-fluid">
              <a href="https://fr-fr.facebook.com/"><img src="assets/facebook.png" alt="lien RS"></a>
            </section> 

            
          </article>

          <article class="col-md-3 col-lg-3 col-xs-12 col-cm-12">

            <section class="container-fluid">
              <a href="https://fr.linkedin.com"><img src="assets/linkedin.png" alt="lien RS"></a>
            </section> 

            
            
          </article>

          <article class="col-md-3 col-lg-3 col-xs-12 col-cm-12">

            <section class="container-fluid">
              <a href="https://twitter.com"><img src="assets/twitter.jpeg" alt="lien RS"></a>
            </section> 
            
          </article>



        </div>
        <div>

      </section>

      <!-- fin footer-->