<?php
/* Code permettant à un admin de supprimer un article*/
  session_start();
  include('all_nav.inc.php'); // Inclure la barre de navigation
  include('all_header.inc.php'); // Inclure l'entête
  include('all_fonction.php');  // Inclure la fonction de connexion

  echo nl2br("\nBienvenue : ".$_SESSION['NOM']." ".$_SESSION['PRENOM']."\n Vous êtes connecté en mode admin \n");

  
  if(isset($_SESSION['message'])) {
    echo '<div class="alert alert-primary" role="alert">';
    echo $_SESSION['message'];
    echo '</div>';
    unset($_SESSION['message']);
  }  
  
    // Connexion :
    $mysqli = ConnexionBDD(); 
  
    // Sécurité de l'url 

  if(!isset($_SESSION['EMAIL']) || $_SESSION['ROLE'] != "5"){
    header("Refresh: 5; url=all_connexion.php");//redirection vers le formulaire de connexion dans 5 secondes
    echo "Vous devez vous connecter pour accéder à l'espace membre.<br><br><i>Redirection en cours, vers la page de connexion...</i>";
    exit(0);//on arrête l'éxécution du reste de la page avec exit, si le membre n'est pas connecté
}

$reponse = $mysqli->query("SELECT * FROM blog");


?>

<!doctype html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Page des blogs</title>

    <!-- css & bootstrap-->

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">

  </head>

  <!-- contenue du site -->

    <body>
    <section class="container-fluid about">
        <div class="container">
        <div class="row">
          <h2 id="about">Les sujets validés</h2>
          <hr class="seperator">
          
          <?php
    while ($donnees = $reponse->fetch_assoc())
    {
        ++$a;
        $_SESSION['TITRE'.$a] = $donnees['TITRE'];
        if ($donnees['ISREADY'] == '1'){
    ?>
          <article class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
            <h2> <?php echo $donnees['TITRE'] ?> </h2>

            <?php
                 if ($donnees['image'] != "image/"){

?>
 <article class="col-md-12 col-lg-12 col-xs-12 col-cm-12">
<section class="container-fluid">
  <a><img src="<?php echo $donnees['image'] ?>"></a>
</section>          
</article>

<?php } ?>
<p> <?php echo $donnees['TEXTE'] ?> </p>

<?php if ($donnees['pdf'] != "pdf/"){ ?>
<a href="<?php echo $donnees['pdf'] ?>">Clique pour télécharger le pdf</a> 
<?php } ?>

<form action="ad_php_delete_blog.php" method="post" >

<div class="row my-3">
 <td><span class="style1"> 
    <input type="submit" name="action" id="supprimer" value=<?php echo "Supprimer".$a ?> /> 
  </span></td> 

</div>
<form>


<hr class="seperator">

<?php }} ?>
        </div>
      </div>

      </section>
      <!-- fin à propos-->

    <body>
<html>

<?php
  include('all_footer.inc.php') // Inclure le bas de page
?>


