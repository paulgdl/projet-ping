<?php
/* Code php permettant la connexion d'un utilisateur */
  session_start(); // Pour les messages

  // récupere les valeurs saisis dans la page inscription
  if(!empty($_POST)){
  extract($_POST);
  $valid = true;
  }
?>

<?php
  // Contenu du formulaire :
  if (isset($_POST['all_connexion.php'])){
    $email =  htmlentities(trim['email']); //, ENT_QUOTES, "UTF-8"); pour injection sql
    $password =  htmlentities(trim['password']);
  }

  include('all_fonction.php'); // Inclure la fonction de connexion   
  $mysqli = ConnexionBDD();

    // Initie la variable qui contiendra la requête
    $Requete = $mysqli->query("SELECT PASS FROM tuteur WHERE EMAIL = '".$email."' AND (ISVALIDATE = '1' OR ISVALIDATE = '3' OR ISVALIDATE = '5')");

    $donnees = $Requete->fetch_assoc();


    $test = password_verify($password, $donnees['PASS']);


    if (!$donnees){
      $_SESSION['message'] =  "Pseudo ou mot de passe incorrect.";
    }
    else{

      if ($test){

        $_SESSION['message'] =  "Autentification réussi";
        $_SESSION['EMAIL'] = $email;

        $reponse = $mysqli->query("SELECT * FROM tuteur WHERE EMAIL = '".$_SESSION['EMAIL']."'");
        $donnees = $reponse->fetch_assoc();
        $_SESSION['ROLE'] = $donnees['ISVALIDATE'];
        $_SESSION['NOM'] = $donnees['NOM'];
        $_SESSION['PRENOM'] = $donnees['PRENOM'];      
      } else{
        $_SESSION['message'] =  "Pseudo ou mot de passe incorrect.";
      }
      
    }
    
                if ($_SESSION['ROLE'] == '1'){

                  header('Location: tt_page_blog.php');

                } elseif ($_SESSION['ROLE'] == '3'){
                  header('Location: pg_page_blog_valider.php');
                }elseif ($_SESSION['ROLE'] == '5'){
                  header('Location: ad_page_delete_blog.php');
                } else{
                  header('Location: all_connexion.php');
                }
?>

<?php
  include('all_footer.inc.php') // Inclure le bas de page
?>
