<?php
/* Code permettant à un tuteur d'ajouter un nouvel article */
  session_start();
  $titre = "Page d'accueil";
  include('all_nav.inc.php'); // Inclure la barre de navigation
  include('all_header.inc.php'); // Inclure l'entête


  if(!isset($_SESSION['EMAIL']) || $_SESSION['ROLE'] != "1"){
    header("Refresh: 5; url=all_connexion.php");//redirection vers le formulaire de connexion dans 5 secondes
    echo "Vous devez vous connecter pour accéder à l'espace membre.<br><br><i>Redirection en cours, vers la page de connexion...</i>";
    exit(0);//on arrête l'éxécution du reste de la page avec exit, si le membre n'est pas connecté
}

// Formulaire permettant de remplir les informations de ce nouvel article
?>


<form action="tt_php_ajout_blog.php" method="post" enctype="multipart/form-data">
  <div class="container">
  
    <div class="row">
      <div class="col-md-12">
        <label for="titre" class="form-label">Titre</label>
        <input type="text" class="form-control " id="titre" name="titre" placeholder="Votre titre..." required>
      </div>

      <div class="col-md-12">
  Select image to upload:
  <input type="file" name="imageToUpload" id="imageToUpload">
    </div>

      <div class="col-md-12">
        <label for="text" class="form-label">Texte</label>
        <input type="text" class="form-control " id="text" name="text" placeholder="Votre texte..." required>
      </div>

      

    </div>

    <div class="col-md-12">
  Select pdf to upload:
  <input type="file" name="pdfToUpload" id="pdfToUpload">
    </div>
    
    <div class="row my-3">
      <div class="d-grid gap-2 d-md-block"><button class="btn btn-outline-primary" type="submit">Soumettre</button></div>   
    </div>



  </div>

</form>

<?php
  include('all_footer.inc.php'); // Inclure le bas de page
?>