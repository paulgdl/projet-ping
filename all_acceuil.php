<?php
/* Code de l'accueil*/
  session_start();
  $titre = "Page d'accueil tuteur";
  include('all_nav.inc.php'); //Inclure la barre de navigation
  include('all_header.inc.php'); // Inclure l'entête


  if(isset($_SESSION['message'])) {
    echo '<div class="alert alert-primary" role="alert">';
    echo $_SESSION['message'];
    echo '</div>';
    unset($_SESSION['message']);
  }
  if(!isset($_SESSION['EMAIL'])){
    header("Refresh: 1; url=connexion.php");//redirection vers le formulaire de connexion dans 5 secondes
    echo "Vous devez vous connecter pour accéder à l'espace membre.<br><br><i>Redirection en cours, vers la page de connexion...</i>";
    exit(0);//on arrête l'éxécution du reste de la page avec exit, si le membre n'est pas connecté
}
?>



<!doctype html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Page d'acceuil</title>

    <!-- css & bootstrap-->

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
    <link rel="stylesheet" href="all_style.css">

  </head>

  <!-- contenue du site -->

  <body>

      <section class="container-fluid about">
        <div class="container">

        <div class="row">
          <h2 id="about">À propos</h2>
          <hr class="seperator">
          <article class="col-md-4 col-lg-4 col-xs-12 col-sm-12">
            <h2>Catégorie de l'article</h2>

            <p> 
            Ici s'affichera le texte écrit par un tuteur correspondant à son article qui aura été validé au préalable

            </p>


          </article>
          <article class="col-md-4 col-lg-4 col-xs-12 col-sm-12">
            <h2>Science</h2>
            <p>
            Découverte des plus anciennes traces de vie sur Terre : Des chercheurs ont découvert les plus anciennes traces d’activité microbienne à la surface de la Terre. Datant de 3,48 milliards d’années, ces stromatolites ne présentent plus aucune signature organique mais une architecture et une composition minéralogique spécifiques. Ce style de biosignature pourrait s’avérer particulièrement intéressant et servir de comparaison pour la recherche de traces de vie sur Mars.

            </p>

          </article>
          <article class="col-md-4 col-lg-4 col-xs-12 col-sm-12">
            <h2>Web</h2>
            <p>
            Netflix lance Triviaverse, un nouveau quiz : comment jouer en duo ou en solo : La plateforme de streaming lance un nouveau jeu interactif pour tester votre culture générale. Triviaverse est un quiz en ligne qui consiste à répondre à une série de questions en un temps limité. Il se joue en solo ou en duo et a pour objectif de faire le meilleur score possible. Sport, musique, science, culture générale… Les questions sont variées et ont plusieurs niveaux de difficulté.
            </p>
            
          </article>

        </div>
      </div>

      </section>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8" crossorigin="anonymous"></script>
  </body>
</html>

    

    
<?php
  header('Location: all_acceuil.php'); // Redirection vers la page d'accueil
  include('all_footer.inc.php') // Inclure le bas de page
?>

