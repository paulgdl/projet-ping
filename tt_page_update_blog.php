<?php
/* Code permettant à un tuteur de modifier son article s'il n'a pas été validé */
  session_start();
  include('all_nav.inc.php'); // Inclure la barre de navigation
  include('all_header.inc.php'); // Inclure l'entête


  if(!isset($_SESSION['EMAIL']) || $_SESSION['ROLE'] != "1"){
    header("Refresh: 5; url=all_connexion.php");//redirection vers le formulaire de connexion dans 5 secondes
    echo "Vous devez vous connecter pour accéder à l'espace membre.<br><br><i>Redirection en cours, vers la page de connexion...</i>";
    exit(0);//on arrête l'éxécution du reste de la page avec exit, si le membre n'est pas connecté
}

// Formulaire permettant de remplir le nouveau texte de l'article
?>

<form  method="POST" action="tt_php_update_blog.php">
  <div class="container">
  
    <div class="row">

    <div class="row">
          <h2 id="about">Modification du blog</h2>
          <hr class="seperator">
   
    
    <div class="col-md-12">
            <h2>
            <?php 
            echo $_SESSION['MODIFICATION']; 
            ?>
            </h2>
            </div>
      <div class="col-md-12">
        <label for="text" class="form-label">Texte</label>
        <input type="text" class="form-control " id="text" name="text" placeholder="Votre texte..." required>
      </div>
    </div>
    <div class="row my-3">
      <div class="d-grid gap-2 d-md-block"><button class="btn btn-outline-primary" type="submit">Soumettre</button></div>   
    </div>
  </div>

</form>



<?php
  include('all_footer.inc.php'); // Inclure le bas de page
?>