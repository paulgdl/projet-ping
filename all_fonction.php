<?php
/* Fonction permettant la connexion à la base de données */
function ConnexionBDD() {
    
    // Connexion :
    require_once("all_param.inc.php");
    $mysqli = new mysqli($host, $login, $passwd, $dbname);
    if ($mysqli->connect_error) {
        die('Erreur de connexion (' . $mysqli->connect_errno . ') '
              . $mysqli->connect_error);
    }
    return $mysqli;
}

?>