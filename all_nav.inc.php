<nav class="navbar navbar-expand-md navbar-dark bg-dark mb-3">
      <div class="container-fluid">
        <a class="navbar-brand" href="index.php">Esigelec</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarText">
          <ul class="navbar-nav me-auto">
            <li class="nav-item">
              <a class="nav-link" aria-current="page" href="index.php">Accueil</a>
            </li>

<?php
            if(isset($_SESSION['EMAIL']) && ($_SESSION['ROLE'] == "1") ){
?>
            
              <li class="nav-item">
              <a class="nav-link" href="tt_page_ajout_blog.php">Créé un blog</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="tt_page_controle_blog.php">Modifier mon blog</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="tt_page_blog.php">Blogs</a>
            </li>
     
<?php
            }
?>

<?php
            if((isset($_SESSION['EMAIL']) && ($_SESSION['ROLE'] == "3"))){
              
?>

            <li class="nav-item">
              <a class="nav-link" href="pg_page_controle_blog.php">Blogs à valider</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="pg_page_valide_compte.php">Valider compte tuteur</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="pg_page_blog_valider.php">Blogs valider</a>
            </li>

<?php
            }
?>

<?php
            if((isset($_SESSION['EMAIL']) && ($_SESSION['ROLE'] == "5"))){
              
?>

            <li class="nav-item">
              <a class="nav-link" href="ad_page_add_ping.php">Ajouter un ping</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="ad_page_delete_blog.php">modération blog</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="ad_page_delete_user.php">Modération user</a>
            </li>

<?php
            }
?>
            
          </ul>

          <ul class="navbar-nav">
          <?php
            if((!isset($_SESSION['EMAIL']))) {
?>
            <li class="nav-item">
              <a class="nav-link" href="all_inscription.php">Inscription</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="all_connexion.php">Connexion</a>
            </li>
<?php
            } else{
?>

          </li>
              <li class="nav-item">
              <a class="nav-link" href="all_deconnexion.php">Deconnexion</a>
            </li>
           
<?php
            
            }
?>


          </ul>
        </div>
      </div>

      
    </nav>